## LDAP protocol for Mountebank

## Usage
Clone this repo and change into its directory. Then run:
```
npm install --no-optional
node_modules/mountebank/bin/mb --configfile imposters.json --protofile protocols.json
```
Then you can run a search query:
```
ldapsearch -H ldap://localhost:3898 -x -b o=iapdir "objectclass=*"
```

## How it works
![Sequence diagram showing data flow between client, custom protocol, and Mountebank.](https://cdn.jsdelivr.net/npm/mountebank-ldap/dataflow.png)
