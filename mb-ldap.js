#!/usr/bin/env node

'use strict';

const fetch = require("node-fetch");
const ldap = require("ldapjs");

if (require.main === module) {
  if (process.argv.length !== 3) {
    console.error(`error Expected 1 argument, got ${process.argv.length - 2}: ${process.argv.slice(2).join(' ')}. Make sure to execute this as createCommand in a mountebank protocol.`);
    process.exit(1);
  }

  let config = JSON.parse(process.argv[2]);
  if (!config.searchBase) {
    console.error(`error No searchBase property in configuration object: ${process.argv[2]}. Define it in your imposter.`);
    process.exit(1);
  }
  config.callbackURL = config.callbackURLTemplate.replace(':port', config.port);

  const app = ldap.createServer();
  app.bind(config.searchBase, function (req, res) {
    res.end();
  });
  app.search(config.searchBase, function (req, res) {
      console.log(`info Search ${JSON.stringify(req.json)}`);

      fetch(config.callbackURL, {
        method: 'post',
        body: JSON.stringify({request: req.json}),
        headers: {'Content-Type': 'application/json'},
      }).then(res => res.json()).then(json => {
        res.send(json.response);
        res.end();
      });
    }
  );
  app.listen(config.port, () => {
    console.log(`info LDAP server listening on port ${config.port}`);
  });
}
